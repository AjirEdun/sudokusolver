<?php

include("src/Sudoku.php");

echo "<pre>";

echo "
    <style>
        .subset {
            padding:5px;
        }
        .subset td {
            padding:10px;
            border:1px solid lightblue;
        }
        .rows td {
            padding:10px;
            border:1px solid lightblue;
        }
        .columns td {
            padding:10px;
            border:1px solid lightblue;
        }
    </style>
";

$sudokuArray = array(
    array(5,3,0,0,7,0,0,0,0),
    array(6,0,0,1,9,5,0,0,0),
    array(0,9,8,0,0,0,0,6,0),
    array(8,0,0,0,6,0,0,0,3),
    array(4,0,0,8,0,3,0,0,1),
    array(7,0,0,0,2,0,0,0,6),
    array(0,6,0,0,0,0,2,8,0),
    array(0,0,0,4,1,9,0,0,5),
    array(0,0,0,0,8,0,0,7,9)
);

$sudoku = new Sudoku($sudokuArray);
echo $sudoku->calculate();

