<?php

require_once("AbstractList.php");

class Column extends AbstractList
{

    /**
     * @return string
     */
    public function display()
    {
        $display = "";
        foreach ($this->list as $key => $element) {
            if ($element == "0") {
                $display .= "<span style='color:grey;'>$element</span> ";
            } else {
                $display .= $element ." ";
            }
            $display .= "<br/>";
        }
        return $display;
    }
}