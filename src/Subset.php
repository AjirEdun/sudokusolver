<?php

require_once("AbstractList.php");

class Subset extends AbstractList
{
    /**
     * @return string
     */
    public function display($text = "")
    {
        $display = "";
        if (!empty($text)) {
            $display = $text . "<br/>";
        }
        $i=1;
        foreach ($this->list as $element) {
            if ($element == "0") {
                $display .= "<span style='color:grey;'>$element</span> ";
            } else {
                $display .= $element." ";
            }
            if ($i%3 == 0) {
                $display .= "<br/>";
            }
            $i++;
        }
        return $display;
    }
}