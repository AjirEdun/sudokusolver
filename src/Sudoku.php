<?php

require_once("Subset.php");
require_once("Row.php");
require_once("Column.php");
require_once("AbstractList.php");


class Sudoku
{

    protected $sudoku;
    protected $subset;
    protected $col;
    protected $row;
    protected $probability;

    public function __construct(array $sudoku)
    {
        $this->sudoku = $sudoku;
        $this->subset = array();
        $this->col = array();
        $this->row = array();
        $this->probability = array();
    }

    public function calculate()
    {
        echo $this->display($this->sudoku,"Sudoku Question");
        $this->handleSubset();
        $this->handleRowsAndColumns();
        $this->displayThings();
        $this->generatePossibilities();
		if ($this->checkWin()) {
			echo "<br/>WIN</br/>";
		} else {
			echo "<br/>LOSE</br/>";
		}
    }

	
	protected function checkWin()
	{
		$win = true;
		foreach($this->subset as $subset) {
			if (!$this->isFull($subset)) {
				$win = false;
				break;
			}
		}
		if ($win) {
			foreach($this->row as $row) {
				if (!$this->isFull($row)) {
					$win = false;
					break;
				}
			}
		}
		
		if ($win) {
			foreach($this->col as $col) {
				if (!$this->isFull($col)) {
					$win = false;
					break;
				}
			}
		}
		
		return $win;
	}
	
	protected function isFull(AbstractList $list)
	{
		$full = true;
		if ($list->exist(0)) {
			$full = false;
		}
	
		return $full;
	}
	
    protected function generatePossibilities()
    {
		
    }

    protected function calculateRowColumSub($x,$y)
    {
        $array = array(
            'row' => $y,
            'column' => $x,
            'subset' => 0
        );

        $horizontal = ceil(($x+1)/3);
        $vertical = ceil(($y+1)/3);
        if ($vertical==1) {
            $subset =$horizontal;
        } else {
            $subset = (($vertical-1)*3)+$horizontal;
        }
        $array['subset'] = $subset;

        return $array;

    }

    protected function handleRowsAndColumns()
    {
        for ($col = 0; $col <9 ; $col++) {
            $row = new Row($this->sudoku[$col]);
            $this->row[] = $row;
            $column = new Column();
            for ($row = 0; $row <9; $row++) {
                $column->add($this->sudoku[$row][$col]);
            }
            $this->col[] = $column;
        }
    }

    protected function handleSubset()
    {
        for ($sub=0; $sub<3; $sub++) {
            for($r=0; $r<3; $r++) {
                $subset = new Subset();
                for ($i=0; $i<3; $i++) {
                    for ($j=0; $j<3; $j++) {
                        $subset->add($this->sudoku[($sub*3)+$i][($r*3)+$j]);
                    }
                }
                $this->subset[] = $subset;
            }
        }
    }


    protected function displayColumns()
    {
        $display = "<h3>Columns</h3><table class='columns'><tbody><tr>";
        foreach ($this->col as $col) {
            $display .= "<td>";
            $display .= $col->display()." ";
            $display .= "</td>";
        }
        $display .= "</tbody></table>";
        $display .= "</tr></tbody></table>";
        return $display;
    }


    /**
     * @return string
     */
    protected function displaySubsets(){
        $display = "<h3>Subsets</h3><table class='subset'><tbody><tr>";
        for ($i=1;$i<10;$i++) {
            $display .= "<td>";
            $display .= $this->subset[$i-1]->display();
            $display .= "</td>";
            if ($i%3 ==0) {
                $display .= "</tr><tr>";
            }
        }
        $display .= "</tr></tbody></table>";

        return $display;
    }

    /**
     * @param array $display
     * @param string $text
     * @return string
     */
    protected function display(array $display, $text = "display",$win = false)
    {
        $displayText =  "------  ".$text." ------ <br/><br/>";
        for ($i=0; $i < count($display); $i++) {
            if (is_array($display[$i])) {
                foreach ($display[$i] as $k => $j) {
                    if ($j == "0") {
                        if ($win) {
                            $displayText .= "<span style='color:red;'>".$j."</span> ";
                        } else {
                            $displayText .= "<span style='color:lightgrey;'>".$j."</span> ";
                        }
                    } else {
                        $displayText .= $j." ";
                    }
                }
            } else {
                $displayText .= $display[$i];
            }
            $displayText .= "<br/>";
        }
        $displayText .= "<br/>------  ".$text." ------<br/><br/><br/>";

        return $displayText;
    }

    protected function displayThings()
    {
        echo "<table><tbody><tr><td>";
        echo $this->displaySubsets();
        echo "</td><td>";
        echo $this->displayRows();
        echo "</td><td>";
        echo $this->displayColumns();
        echo "</td></tr></tbody></table>";
    }

    /**
     * @return string
     */
    protected function displayRows()
    {
        $display = "<h3>Rows</h3><table class='rows'><tbody>";
        foreach ($this->row as $row) {
            $display .= "<tr><td>";
            $display .= $row->display()." ";
            $display .= "</td></tr>";
        }
        $display .= "</tbody></table>";

        return $display;
    }
}
