<?php

class AbstractList
{

    /**
     * @var array
     */
    protected $list;

    /**
     * @param array $list
     */
    public function __construct(array $list = array())
    {
        $this->list = $list;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param $number
     */
    public function add($number)
    {
        if (!in_array($number,$this->list) || $number==0) {
            $this->list[] = $number;
        }
    }



    /**
     * @param $number
     * @return bool
     */
    public function exist($number)
    {
        return in_array($number,$this->list);
    }

    /**
     * @return array
     */
    public function getMissing()
    {
        $all = array(1,2,3,4,5,6,7,8,9,0);

        return array_diff($this->list,$all);
    }

    /**
     * @return string
     */
    public function display()
    {
        $display = "";
        foreach ($this->list as $key => $element) {
            if ($element == "0") {
                $display .= "<span style='color:grey;'>$element</span> ";
            } else {
                $display .= $element ." ";
            }
        }
        return $display;
    }

}